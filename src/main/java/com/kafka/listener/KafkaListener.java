package com.kafka.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class KafkaListener {

	@org.springframework.kafka.annotation.KafkaListener(id = "container1", topics = "${spring.kafka.consumer.topic}", containerFactory = "listenerContainerFactory")
	public void consume(String message) {
		log.info("Message recieved from Kafka topic :{} " , message);
	}

	@org.springframework.kafka.annotation.KafkaListener(id = "container2", topics = "${spring.kafka.consumer.topic}", containerFactory = "listenerContainerFactory")
	public void consume(ConsumerRecord record) {
		log.info("Message recieved from Kafka topic :{} ", record.value());
	}
}
