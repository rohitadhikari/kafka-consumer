package com.kafka.contoller;

import java.util.Properties;
import java.util.function.Supplier;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/kafka")
@Slf4j
public class KafkaController {

	@Autowired
	private KafkaListenerEndpointRegistry registry;
	
	//@Autowired
	//private DefaultKafkaConsumerFactory<?, ?> factory;
	
	@GetMapping("/containers")
	public ResponseEntity<?> getContainers() {
		registry.getListenerContainerIds();
		return ResponseEntity.ok(registry.getListenerContainerIds());
	}

	@GetMapping("/status")
	public ResponseEntity<?> getContainerStatus() {
		return ResponseEntity.ok(registry.isRunning());
	}

	@GetMapping("/stop")
	public ResponseEntity<?> stopContainer() {
		if (registry.isRunning()) {
			registry.stop();
		}
		return ResponseEntity.ok(registry.isRunning());
	}

	@GetMapping("/start")
	public ResponseEntity<?> startContainer() {
		if (!registry.isRunning()) {
			registry.start();
		}
		return ResponseEntity.ok(registry.isRunning());
	}

	@GetMapping("/restart")
	public ResponseEntity<?> restartContainer() {
		log.info("Restarting container");
		if (registry.isRunning()) {
			registry.stop();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				log.error(e.getMessage(), e);
			}
		}
		registry.start();
		return ResponseEntity.ok(registry.isRunning());
	}
	
	/**
	 * Repoint a kafka container towards a new IP address.
	 * 
	 * @param containerId
	 * @param bootstrapServer
	 * @return
	 */
	@GetMapping("/switch/{containerId}")
	public ResponseEntity<?> poc(@PathVariable("containerId")String containerId, @RequestParam("bootstrap-servers")String bootstrapServer) {
		log.info("Restarting container {} , with new bootstrap-server {}", containerId, bootstrapServer);
		//fetch the required container
		MessageListenerContainer container = registry.getListenerContainer(containerId);
			
		//init new properties on container
		Properties kafkaConsumerProperties = new Properties();
		kafkaConsumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
		container.getContainerProperties().setKafkaConsumerProperties(kafkaConsumerProperties);
		
		//log.info("after :"+factory.getConfigurationProperties().get(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG));
		//Supplier<String> s = () -> bootstrapServer;
		//log.info(s.get());
		//factory.setBootstrapServersSupplier(s);
		//log.info("after :"+factory.getConfigurationProperties().get(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG));
		
		//restart the container
		restartContainer();
		return ResponseEntity.ok(container.getContainerProperties());
	}
}
